# Chef Server

CentOS docker container, Chef Server installation and configuration

## Description

The container provides a complete Chef Server starting from a CentOS image.

## Exposed port(s)

Chef Server on SSL port `443`

## Prepare volumes on docker server

```bash
sudo mkdir /opt/docker-volumes/
sudo mkdir /opt/docker-volumes/chef
sudo mkdir /opt/docker-volumes/chef/chef-log
sudo mkdir /opt/docker-volumes/chef/chef-data
```

## Run centos/systemd container with privileged permissions and ipv6 disabled

```bash
sudo docker run --sysctl net.ipv6.conf.all.disable_ipv6=1 --privileged --name chef-server-core -d -v /opt/docker-volumes/chef/chef-data:/var/opt/opscode -v /opt/docker-volumes/chef/chef-log:/var/log/opscode -p 80:80 -p 443:443 centos/systemd
```

## Connect to your new container

```bash
sudo docker exec -it chef-server-core bash
```

## Download Chef

```bash
curl https://packages.chef.io/files/stable/chef-server/13.0.17/el/7/chef-server-core-13.0.17-1.el7.x86_64.rpm -o chef-server-core.rpm
rpm -Uvh chef-server-core.rpm
yum install rsync crontabs which net-tools less -y
systemctl enable crond
systemctl start crond
```

## Fix listen address

```bash
yum install nano
nano /opt/opscode/embedded/cookbooks/private-chef/attributes/default.rb
```

change this line

```
default['private_chef']['postgresql']['listen_address'] = "localhost"
```

## Add SSL Certificates

Chef Server will create a self signed certificate using the instance ID as hostname.
This could be - possibly - be fine for you, but it's recommended to use a legit SSL certificate.

Copy yourcert.crt in /etc/pki/tls/certs/
Copy yourcert.key in /etc/pki/tls/private/
Edit /etc/opscode/chef-server.rb


```
nginx['ssl_certificate'] = "/etc/pki/tls/certs/all.softsolutions.it.crt"
nginx['ssl_certificate_key'] = "/etc/pki/tls/private/all.softsolutions.it.key"
```

## Configure Chef Server

```bash
chef-server-ctl reconfigure
```


## Install chef-manage

```bash
chef-server-ctl install chef-manage
chef-server-ctl reconfigure
chef-manage-ctl reconfigure --accept-license
```

## Creat chef admin

Admin.pem is the public key for admin user. Keep it safe, it will be needed later.


```bash
chef-server-ctl user-create USERNAME FIRST_NAME LAST_NAME EMAIL 'PASSWORD' --filename /root/admin.pem
```


## Create organization

This command will create your first organization and link the admin user to it.

```bash
chef-server-ctl org-create SHORT_NAME 'FULL_NAME' --association_user admin --filename /root/admin.pem
```

##  CNAME

Create a CNAME entry in your DNS accordingly with the SSL certificate you used.








Launch the container

```bash
sudo docker run -d --name="smtprelay" \
    -e SMTP_LOGIN=<Amazon SES smtp username> \
    -e SMTP_PASSWORD=<Amazon SES smtp password> \
    -p 25:25 \
    smtp-relay
```

Create SystemD service

```bash
sudo nano /etc/systemd/system/docker-smtprelay.service
```

```
[Unit]
Description=SMTP Relay Container
After=docker.service
Requires=docker.service
 
[Service]
Restart=always
ExecStart=/usr/bin/docker start -a smtprelay
ExecStop=/usr/bin/docker stop -t 2 smtprelay
 
[Install]
WantedBy=multi-user.target
```

Note:
*Restart=always* will make sure the service is always running. This means you cannot stop the container using docker commands.

```bash
sudo systemctl daemon-reload
sudo systemctl start docker-smtprelay
sudo systemctl enable docker-smtprelay
```

The smtprelay container will now start automatically with your Ubuntu server.

To check if it's running
```bash
sudo docker ps
```

To check the container performances
```bash
sudo docker stats
```

To check container logs
```bash
sudo docker logs -f smtprelay
```

To access container shell
```bash
sudo docker exec -ti smtprelay /bin/sh
```

To stop the container
```bash
sudo systemctl stop docker-smtprelay
```

To start the container
```bash
sudo systemctl start docker-smtprelay
```

To disable the SystemD Service
```bash
sudo systemctl disable docker-smtprelay
```


# Client configuration

In this section we'll assume you already installed CHEF-CLIENT and you're working locally with a folder structure like this

```
C:\Chef
.......\cookbooks
.......\environments
.......\roles
.......\trusted:certs
.....................\yourcert.crt
.....................\ca.boundle.pem
. knife.rb
. admin.pem
. validation.pem
```

## What are those files?

all.softsolutions.it.crt e ca-bundle.pem

Vanno copiati in C:\chef\trusted_certs e servono per la connessione SSL al server.


**admin.pem**

Private key for your admin user


**validation.pem**

Private key for your organization


**knife.rb**

Chef/Knife configuration file

```
current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "admin"
client_key               "#{current_dir}/admin.pem"
chef_server_url          "https://yourchefserver/organizations/yourorganization"
cookbook_path            ["#{current_dir}/cookbooks"]
```